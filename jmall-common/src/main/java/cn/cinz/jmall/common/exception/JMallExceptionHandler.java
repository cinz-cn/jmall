package cn.cinz.jmall.common.exception;

import cn.cinz.jmall.common.enums.error.BizCodeEnum;
import cn.cinz.jmall.common.utils.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局异常处理器
 * @author zengcheng
 */
@RestControllerAdvice // 为了返回数据，并标识这是一个全局异常捕获类
public class JMallExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(JMallException.class)
	public R handleRRException(JMallException e){
		R r = new R();
		r.put("code", e.getCode());
		r.put("msg", e.getMessage());
		logger.error(e.getMessage());
		e.printStackTrace();
		return r;
	}

	/**
	 * 当对带有@Valid注释的参数的验证失败时抛出异常。
	 *
	 * @param e
	 * @return 参数检验失败的错误消息
	 */
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public R handleRRException(MethodArgumentNotValidException e){
		logger.error("数据校验出现问题{}，异常类型：{}", e.getMessage(), e.getClass());
		// 返回验证失败的结果
		BindingResult result = e.getBindingResult();
		Map<String, String> errorMap = new HashMap<>();
		// 获取数据的字段名和数据出错的默认消息
		result.getFieldErrors().forEach(error -> {
			errorMap.put(error.getField(),error.getDefaultMessage());
		});

		return R.error(BizCodeEnum.VALID_EXCPTION.getCode(), BizCodeEnum.VALID_EXCPTION.getMsg()).put("data", errorMap);
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public R handlerNoFoundException(Exception e) {
		logger.error(e.getMessage(), e);
		return R.error(404, "路径不存在，请检查路径是否正确");
	}

	@ExceptionHandler(DuplicateKeyException.class)
	public R handleDuplicateKeyException(DuplicateKeyException e){
		logger.error(e.getMessage(), e);
		return R.error("数据库中已存在该记录");
	}

//	@ExceptionHandler(AuthorizationException.class)
//	public R handleAuthorizationException(AuthorizationException e){
//		logger.error(e.getMessage(), e);
//		return R.error("没有权限，请联系管理员授权");
//	}

	@ExceptionHandler(Exception.class)
	public R handleException(Exception e){
		logger.error(e.getMessage(), e);
		return R.error();
	}
}
