package cn.cinz.jmall.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * MP 自动填充功能
 *
 * @Description: MP 自动填充功能
 * @Author zengcheng
 * @Date 2022/2/28 19:24
 * @Version 1.0
 */
@Configuration//让Spring扫描到
public class MyMetaObjectHandler implements MetaObjectHandler {

    //使用mp实现添加操作时，这个方法执行
    @Override
    public void insertFill(MetaObject metaObject) {
        //set 创建时间
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("createDate", new Date(), metaObject);
        this.setFieldValByName("gmtCreate", new Date(), metaObject);

        this.setFieldValByName("deleted", 0, metaObject);
        this.setFieldValByName("show_status", 0, metaObject);
        this.setFieldValByName("version", 1, metaObject);

    }

    //使用mp实现修改操作时，这个方法执行
    @Override
    public void updateFill(MetaObject metaObject) {
        //set 修改时间
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("updateDate", new Date(), metaObject);
        this.setFieldValByName("gmtModified", new Date(), metaObject);
    }
}
