package cn.cinz.jmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients //开启服务调用
public class JmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallCouponApplication.class, args);
    }

}

