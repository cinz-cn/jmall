package cn.cinz.jmall.coupon.dao;

import cn.cinz.jmall.coupon.entity.CouponHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券领取历史记录
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 16:07:55
 */
@Mapper
public interface CouponHistoryDao extends BaseMapper<CouponHistoryEntity> {
	
}
