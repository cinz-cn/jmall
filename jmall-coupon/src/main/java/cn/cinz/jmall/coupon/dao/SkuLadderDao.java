package cn.cinz.jmall.coupon.dao;

import cn.cinz.jmall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 16:07:55
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
