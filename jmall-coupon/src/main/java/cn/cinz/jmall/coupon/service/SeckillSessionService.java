package cn.cinz.jmall.coupon.service;

import cn.cinz.jmall.coupon.entity.SeckillSessionEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 16:07:55
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

