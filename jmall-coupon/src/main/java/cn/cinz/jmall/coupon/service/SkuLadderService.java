package cn.cinz.jmall.coupon.service;

import cn.cinz.jmall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 16:07:55
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

