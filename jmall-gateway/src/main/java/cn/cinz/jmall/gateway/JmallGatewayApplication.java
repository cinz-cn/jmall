package cn.cinz.jmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) //排除数据库自动配置
public class JmallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallGatewayApplication.class, args);
    }

}
