package cn.cinz.jmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients //开启服务调用
public class JmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallMemberApplication.class, args);
    }

}
