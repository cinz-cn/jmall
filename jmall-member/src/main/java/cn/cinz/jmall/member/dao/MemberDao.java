package cn.cinz.jmall.member.dao;

import cn.cinz.jmall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 18:10:50
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
