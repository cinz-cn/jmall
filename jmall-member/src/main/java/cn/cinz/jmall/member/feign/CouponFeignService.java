package cn.cinz.jmall.member.feign;

import cn.cinz.jmall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @FeignClient(value = "vod-api", path = "/api/aliyun/vod/video") // value = ??????? , path = ????@RequestMapping, fallback = ????????class
 */
@FeignClient(value = "coupon-api", path = "/coupon", fallback = CouponFeignServiceImpl.class)
public interface CouponFeignService {
    @RequestMapping("/member/list")
    public R memberCoupons();
}
