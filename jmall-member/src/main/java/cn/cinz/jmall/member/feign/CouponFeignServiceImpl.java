package cn.cinz.jmall.member.feign;

import cn.cinz.jmall.common.utils.R;
import org.springframework.stereotype.Component;

@Component
public class CouponFeignServiceImpl implements CouponFeignService{
    @Override
    public R memberCoupons() {
        return R.error();
    }


}
