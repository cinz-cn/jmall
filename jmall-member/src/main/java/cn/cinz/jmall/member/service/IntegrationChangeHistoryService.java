package cn.cinz.jmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 18:10:50
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

