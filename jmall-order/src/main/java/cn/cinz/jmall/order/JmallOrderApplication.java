package cn.cinz.jmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallOrderApplication.class, args);
    }

}
