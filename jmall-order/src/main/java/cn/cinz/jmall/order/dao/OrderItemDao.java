package cn.cinz.jmall.order.dao;

import cn.cinz.jmall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:09:50
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
