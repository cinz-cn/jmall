package cn.cinz.jmall.order.dao;

import cn.cinz.jmall.order.entity.OrderReturnReasonEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退货原因
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:09:50
 */
@Mapper
public interface OrderReturnReasonDao extends BaseMapper<OrderReturnReasonEntity> {
	
}
