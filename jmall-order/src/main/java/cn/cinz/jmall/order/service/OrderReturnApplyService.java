package cn.cinz.jmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.order.entity.OrderReturnApplyEntity;

import java.util.Map;

/**
 * 订单退货申请
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:09:50
 */
public interface OrderReturnApplyService extends IService<OrderReturnApplyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

