package cn.cinz.jmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:09:50
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

