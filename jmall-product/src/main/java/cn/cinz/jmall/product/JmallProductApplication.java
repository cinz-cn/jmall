package cn.cinz.jmall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"cn.cinz.jmall"})//全局扫描配置类
@MapperScan("cn.cinz.jmall.product.dao")
@SpringBootApplication
public class JmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallProductApplication.class, args);
    }

}
