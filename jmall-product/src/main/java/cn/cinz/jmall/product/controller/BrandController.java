package cn.cinz.jmall.product.controller;

import java.util.Arrays;
import java.util.Map;

import cn.cinz.jmall.common.valid.AddGroup;
import cn.cinz.jmall.common.valid.UpdateGroup;
import cn.cinz.jmall.common.valid.UpdateShowStatusGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.cinz.jmall.product.entity.BrandEntity;
import cn.cinz.jmall.product.service.BrandService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.common.utils.R;

import javax.validation.Valid;


/**
 * 品牌
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-20 21:45:10
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);



        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@Validated(value = {AddGroup.class}) @RequestBody BrandEntity brand){
        brandService.save(brand);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@Validated(value = {UpdateGroup.class}) @RequestBody BrandEntity brand){
		brandService.updateByIdDetail(brand);

        return R.ok();
    }

    @PostMapping("/update/show-status")
    public R updateShowStatus(@Validated(value = {UpdateShowStatusGroup.class}) @RequestBody BrandEntity brand){
        brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
