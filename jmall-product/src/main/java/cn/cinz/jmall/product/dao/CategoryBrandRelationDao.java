package cn.cinz.jmall.product.dao;

import cn.cinz.jmall.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-20 21:45:10
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

    /**
     * 修改分类
     * @param catId
     * @param catelogName
     */
    void updateCategory(@Param("catId") Long catId, @Param("catelogName") String catelogName);
}
