package cn.cinz.jmall.product.dao;

import cn.cinz.jmall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-20 21:45:10
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
