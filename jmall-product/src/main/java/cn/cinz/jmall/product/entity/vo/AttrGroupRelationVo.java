package cn.cinz.jmall.product.entity.vo;

import lombok.Data;

@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
