package cn.cinz.jmall.product.service;

import cn.cinz.jmall.product.entity.AttrGroupEntity;
import cn.cinz.jmall.product.entity.vo.AttrGroupRelationVo;
import cn.cinz.jmall.product.entity.vo.AttrResultVo;
import cn.cinz.jmall.product.entity.vo.AttrVo;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-20 21:45:10
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType);

    /**
     * 获取 商品-规格参数-基本信息
     * @param attrId
     */
    AttrResultVo getAttrInfo(Long attrId);

    /**
     * 修改商品属性 与 属性&属性分组关联
     * @param attrVo
     */
    void updateAttr(AttrVo attrVo);

    /**
     * 根据分组id查询关联的所有属性
     * @param attrgroupId
     * @return
     */
    List<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteRelation(AttrGroupRelationVo[] relationVo);

    /**
     * 获取当前分组没有关联的所有属性
     * @param params
     * @param attrgroupId
     * @return
     */
    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);
}

