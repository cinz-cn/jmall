package cn.cinz.jmall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmallThirdPartyApplication.class, args);
    }


}
