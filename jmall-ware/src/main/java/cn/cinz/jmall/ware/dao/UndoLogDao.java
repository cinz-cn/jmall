package cn.cinz.jmall.ware.dao;

import cn.cinz.jmall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:15:09
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
