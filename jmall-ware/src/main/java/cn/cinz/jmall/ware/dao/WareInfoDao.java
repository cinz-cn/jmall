package cn.cinz.jmall.ware.dao;

import cn.cinz.jmall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:15:09
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
