package cn.cinz.jmall.ware.dao;

import cn.cinz.jmall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:15:09
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
