package cn.cinz.jmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:15:09
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

