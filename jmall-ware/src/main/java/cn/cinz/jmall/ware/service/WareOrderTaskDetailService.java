package cn.cinz.jmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.cinz.jmall.common.utils.PageUtils;
import cn.cinz.jmall.ware.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author zengcheng
 * @email zeng7763@gmail.com
 * @date 2022-07-21 19:15:09
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

